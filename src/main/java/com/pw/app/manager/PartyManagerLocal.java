/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pw.app.manager;

import com.pw.app.pojo.AppParty;
import com.pw.app.pojo.PartyPayload;
import com.pw.app.util.exception.GeneralAppException;
import java.util.List;

/**
 *
 * @author buls
 */
public interface PartyManagerLocal {
    
    PartyPayload getParties() throws GeneralAppException;
    
    List<AppParty> getAppParties() throws GeneralAppException;
    
}