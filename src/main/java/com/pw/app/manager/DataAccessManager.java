/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pw.app.manager;

import com.pw.app.data.manager.DataAccessDataManagerLocal;
import com.pw.app.data.manager.ExceptionThrowerManagerLocal;
import com.pw.app.model.DeviceToken;
import com.pw.app.model.User;
import com.pw.app.pojo.AppBoolean;
import com.pw.app.pojo.AppUser;
import com.pw.app.pojo.UserPayload;
import com.pw.app.util.CodeGenerator;
import com.pw.app.util.JWT;
import com.pw.app.util.JikaDecode;
import com.pw.app.util.MD5;
import com.pw.app.util.SocialPlatformType;
import com.pw.app.util.Verifier;
import java.util.Calendar;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import com.pw.app.util.exception.GeneralAppException;
import io.jsonwebtoken.Claims;
import java.util.ArrayList;
import java.util.logging.Logger;
import com.pw.app.data.manager.UserDataManagerLocal;
import com.pw.app.model.DataAccess;
import com.pw.app.pojo.AppDataAccess;

/**
 *
 * @author Lateefah
 */
@Stateless
public class DataAccessManager implements DataAccessManagerLocal {

    @EJB
    private DataAccessDataManagerLocal dataAccessDataManager;
    
    @EJB
    private ExceptionThrowerManagerLocal exceptionManager;

    @EJB
    private DeviceTokenManagerLocal deviceTokenManager;
    
    @EJB
    private CodeGenerator codeGenerator;
    
    @EJB
    private Verifier verifier;
    
    private final String USER_LINK = "/user";
    private final String AUTHENTICATE_USER_LINK = "/user/authenticate";
    private final String VERIFY_USERNAME_LINK = "/user/verify";
    private final String CREATE_USERNAME_LINK = "/user/username";
    private final long TOKEN_LIFETIME= 31556952000l;
    private final long PUBLISH_TIMEOUT = 1000;
    
    private Logger logger = Logger.getLogger(DataAccessManager.class.getName());
  
    @Override
    public List<AppDataAccess> getDataAccess(String username) {
        List<DataAccess> userDataAccessList = dataAccessDataManager.getByUsername(username);
        List<AppDataAccess> appUserDataAccess = getAppDataAccess(userDataAccessList);
        
        return appUserDataAccess;
    }
      
    
    private List<AppDataAccess> getAppDataAccess(List<DataAccess> dataAccessList) {
        List<AppDataAccess> appDataAccessList = new ArrayList<AppDataAccess>();
        for (DataAccess dataAccess : dataAccessList) {
            logger.info(dataAccess.getElectionType() + " " + dataAccess.getElectionId());
            AppDataAccess appDataAccess = getAppDataAccess(dataAccess);
            appDataAccessList.add(appDataAccess);
        }
        
        return appDataAccessList;
    }
    
    private AppDataAccess getAppDataAccess(DataAccess dataAccess) {
        AppDataAccess appDataAccess = new AppDataAccess();
        
        if (dataAccess != null) {
            appDataAccess.setId(dataAccess.getId());
            appDataAccess.setAgentId(dataAccess.getAgentId());
            appDataAccess.setUsername(dataAccess.getUsername());
            appDataAccess.setPuId(dataAccess.getPuId());
            appDataAccess.setElectionId(dataAccess.getElectionId());
            appDataAccess.setElectionType(dataAccess.getElectionType());
            appDataAccess.setElectionDate(dataAccess.getElectionDate());
        }
        
        return appDataAccess;
    }            

}
