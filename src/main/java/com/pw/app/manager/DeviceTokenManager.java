/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pw.app.manager;

import com.pw.app.data.manager.DeviceTokenDataManagerLocal;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import com.pw.app.model.DeviceToken;

/**
 *
 * @author buls
 */
@Stateless
public class DeviceTokenManager implements DeviceTokenManagerLocal{

    @EJB
    DeviceTokenDataManagerLocal deviceTokenDataManager;

    @Override
    public DeviceToken getDeviceToken(String userId) {
        return deviceTokenDataManager.get(userId);
    }

    @Override
    public DeviceToken updateDeviceToken(DeviceToken deviceToken) {
        return deviceTokenDataManager.update(deviceToken);                
    }

    @Override
    public void deleteDeviceToken(DeviceToken deviceToken) {
        deviceTokenDataManager.delete(deviceToken);
    }

    @Override
    public DeviceToken createDeviceToken(DeviceToken deviceToken) {
        return deviceTokenDataManager.create(deviceToken);
    }
    
    
    
}
