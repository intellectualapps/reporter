/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pw.app.manager;

import com.pw.app.pojo.AppDataAccess;
import java.util.List;

/**
 *
 * @author buls
 */
public interface DataAccessManagerLocal {
 
    
    List<AppDataAccess> getDataAccess (String username);
    
}