/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pw.app.manager;

import com.pw.app.data.manager.PartyDataManagerLocal;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import com.pw.app.util.exception.GeneralAppException;
import java.util.ArrayList;
import java.util.logging.Logger;
import com.pw.app.model.Party;
import com.pw.app.pojo.AppParty;
import com.pw.app.pojo.PartyPayload;

/**
 *
 * @author buls
 */
@Stateless
public class PartyManager implements PartyManagerLocal {

    @EJB
    private PartyDataManagerLocal partyDataManager;        
    
    private final String PROJECT_LINK = "/party";
    private final long TOKEN_LIFETIME= 31556952000l;
    private final long PUBLISH_TIMEOUT = 1000;
    
    private Logger logger = Logger.getLogger(PartyScoreManager.class.getName());

    @Override
    public PartyPayload getParties() throws GeneralAppException {
        List<Party> parties = partyDataManager.get();
        List<AppParty> appParties = getAppParties(parties);
        
        PartyPayload partyPayload = new PartyPayload();
        partyPayload.setParties(appParties);
        
        return partyPayload;
    }
    
    private List<AppParty> getAppParties(List<Party> parties) {
        List<AppParty> appParties = new ArrayList<AppParty>();
        for (Party party : parties) {
            AppParty appParty = getAppParty(party);
            appParties.add(appParty);
        }
        
        return appParties;
    }
    
    private AppParty getAppParty(Party party) {
        AppParty appParty = new AppParty();
        if (party != null) {
            appParty.setPartyId(party.getPartyId());
            appParty.setName(party.getName());
            appParty.setDescription(party.getDescription());
            appParty.setDateCreated(party.getDateCreated());
            appParty.setDateModified(party.getDateModified());            
        }
        
        return appParty;
    }

    @Override
    public List<AppParty> getAppParties() throws GeneralAppException {
        List<Party> parties = partyDataManager.get();
        List<AppParty> appParties = getAppParties(parties);
        
        return appParties;
    }
        
   
}
