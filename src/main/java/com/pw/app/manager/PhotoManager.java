/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pw.app.manager;

import com.pw.app.data.manager.ExceptionThrowerManagerLocal;
import com.pw.app.model.DeviceToken;
import com.pw.app.model.User;
import com.pw.app.pojo.AppBoolean;
import com.pw.app.pojo.AppUser;
import com.pw.app.pojo.UserPayload;
import com.pw.app.util.CodeGenerator;
import com.pw.app.util.JWT;
import com.pw.app.util.JikaDecode;
import com.pw.app.util.MD5;
import com.pw.app.util.SocialPlatformType;
import com.pw.app.util.Verifier;
import java.util.Calendar;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import com.pw.app.util.exception.GeneralAppException;
import io.jsonwebtoken.Claims;
import java.util.ArrayList;
import java.util.logging.Logger;
import com.pw.app.data.manager.UserDataManagerLocal;
import com.pw.app.http.ServiceConsumer;
import com.pw.app.pojo.AppDataAccess;
import com.pw.app.pojo.AppParty;
import com.pw.app.pojo.UserDataAccessPayload;
import java.io.IOException;

/**
 *
 * @author Lateefah
 */
@Stateless
public class PhotoManager implements PhotoManagerLocal {
    
    @EJB
    private Verifier verifier;
    
    private final String PHOTO_LINK = "/photo";
    
    private Logger logger = Logger.getLogger(PhotoManager.class.getName());

    @Override
    public AppBoolean savePhotoData(String projectId, String resultSheetPhoto, 
            String electionPhoto1, String electionPhoto2) throws GeneralAppException {
        verifier.setResourceUrl(PHOTO_LINK)
                .verifyParams(projectId);        
        
        AppBoolean appBoolean = new AppBoolean();
        appBoolean.setStatus(Boolean.FALSE);
        
        if (resultSheetPhoto == null || resultSheetPhoto.isEmpty()) {
            resultSheetPhoto = "0";
        }
        
        if (electionPhoto1 == null || electionPhoto1.isEmpty()) {
            electionPhoto1 = "0";
        }
        
        if (electionPhoto2 == null || electionPhoto2.isEmpty()) {
            electionPhoto2 = "0";
        }        
        
        try {
            String savePhotoResponse = new ServiceConsumer().savePhoto(projectId,
                    resultSheetPhoto, electionPhoto1, electionPhoto2);
            appBoolean.setStatus(Boolean.TRUE);
        } catch (IOException ioe) {
            logger.info("SAVE PHOTO ERROR: " + ioe.getMessage());
        }
        
        return appBoolean;
    }          
    
}
