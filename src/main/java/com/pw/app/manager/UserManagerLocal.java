/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pw.app.manager;

import com.pw.app.model.DeviceToken;
import com.pw.app.model.User;
import com.pw.app.pojo.AppBoolean;
import com.pw.app.pojo.AppUser;
import com.pw.app.pojo.UserDataAccessPayload;
import com.pw.app.pojo.UserPayload;
import com.pw.app.util.exception.GeneralAppException;

/**
 *
 * @author Lateefah
 */
public interface UserManagerLocal {
 
    
    //AppUser getAppUser (User user);
    
    AppUser getUserDetails (String username, String rawToken) throws GeneralAppException; 
    
    AppUser getUserDetailsWithDeviceToken (String username, String rawToken) throws GeneralAppException; 
    
    AppUser getUserDetails (String rawToken) throws GeneralAppException; 
    
    AppUser register(String username, String email, String password) throws GeneralAppException;
    
    AppUser updateUserDetails(String username, String email, String firstName, String lastName,
                                String phoneNumber, String photoUrl, String location, String facebookEmail,
                                String linkedinEmail, String shortBio, String longBio, String facebookProfile, 
                                String linkedinProfile, String receiveEmails, String receivePushNotifications, 
                                String rawToken) throws GeneralAppException;
    
    AppBoolean deleteUser(String username) throws GeneralAppException;
    
    UserDataAccessPayload authenticate (String id, String password, String type) throws GeneralAppException;
    
    AppBoolean verifyUsername(String username) throws GeneralAppException;
            
    AppUser createUsername(String username, String email, String tempKey) throws GeneralAppException;
    
    AppUser linkSocialAcount(String username, String platform, String email, String rawToken) throws GeneralAppException;
    
    AppUser unlinkSocialAcount(String username, String platform, String rawToken) throws GeneralAppException;
    
    UserPayload getListOfUsers (String usernames, String rawToken) throws GeneralAppException;
    
    UserPayload getListOfUsersWithDeviceToken (String usernames, String rawToken) throws GeneralAppException;
        
    void notifyOfflineUser(String offlineUsername, String onlineUsername, String connectionId, 
            String rawToken, String notificationPlatform) throws GeneralAppException;
        
    DeviceToken saveDeviceToken(String username, String deviceToken, String uuid, 
            String platform, String rawToken)
            throws GeneralAppException;
    
    AppUser resetPassword (String username) throws GeneralAppException;
    
    AppUser setNewPassword (String username, String password) throws GeneralAppException;
    
    String authenticatePasswordResetToken(String encodedEmail, String token) throws GeneralAppException;
}