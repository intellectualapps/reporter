/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pw.app.manager;

import com.pw.app.model.DeviceToken;
/**
 *
 * @author buls
 */
public interface DeviceTokenManagerLocal {
    
    DeviceToken getDeviceToken(String userId);
    
    DeviceToken updateDeviceToken(DeviceToken deviceToken);
    
    void deleteDeviceToken(DeviceToken deviceToken);
    
    DeviceToken createDeviceToken(DeviceToken deviceToken);
    
}
