/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pw.app.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Lateefah
 */

@Entity
@Table(name = "agents")
@XmlRootElement
@NamedQueries({
  @NamedQuery(name = "User.findByUsername", query = "SELECT u FROM User u WHERE u.username = :username ")})

public class User implements Serializable {
    @Id
    @NotNull    
    @Column(name = "id")
    private String id;
    
    @Column(name = "username")
    private String username;    
        
    @Column(name = "password")
    private String password;
    
    @NotNull
    @Temporal(TemporalType.DATE)
    @Column(name = "created")
    private Date creationDate;
    
    @NotNull
    @Temporal(TemporalType.DATE)
    @Column(name = "modified")
    private Date modificationDate;
    
    public User() { }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }        
    
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }    
    
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    public Date getCreationDate() {
        return creationDate;
    }
    
    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }                

    public Date getModificationDate() {
        return modificationDate;
    }

    public void setModificationDate(Date modificationDate) {
        this.modificationDate = modificationDate;
    }
        
}
