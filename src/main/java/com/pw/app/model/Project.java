/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pw.app.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author buls
 */

@Entity
@Table(name = "projects")
@XmlRootElement

public class Project implements Serializable {
    @Id
    @NotNull    
    @Column(name = "id")
    private String id;

    @Column(name = "agentid")
    private String agentId;

    @Column(name = "puid")
    private Integer puId;
    
    @Column(name = "electionid")
    private Integer electionId;
    
    @Column(name = "geopoint_latitude")
    private Double latitude;

    @Column(name = "geopoint_longitude")
    private Double longitude;    
        
    @Column(name = "geopoint_altitude")
    private Double altitude;
    
    @Column(name = "geopoint_accuracy")
    private Double accuracy;
    
    @Column(name = "ec8a_url")
    private String ec8aUrl;
    
    @Column(name = "registered_voters")
    private Integer registeredVoters;
    
    @Column(name = "accredited_voters")
    private Integer accreditedVoters;
    
    @Column(name = "valid_voters")
    private Integer validVotes;
    
    @Column(name = "rejected_votes")
    private Integer rejectedVotes;
    
    @Column(name = "total_votes_cast")
    private Integer totalVotesCast;
    
    @Column(name = "deviceid")
    private String deviceId;
    
    @Column(name = "simserial")
    private String simSerial;
    
    @Column(name = "meta_instanceID")
    private String metaInstanceId;
    
    @NotNull
    @Column(name = "created")
    private Timestamp dateCreated;
    
    @NotNull
    @Column(name = "modified")
    private Timestamp dateModified;
    
    @NotNull
    @Column(name = "start_date")
    private Timestamp startDate;
    
    @NotNull
    @Column(name = "end_date")
    private Timestamp endDate;
        
    @NotNull
    @Column(name = "submission_date")
    private Timestamp submissionDate;    
            
    public Project() { }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }   

    public Integer getPuId() {
        return puId;
    }

    public void setPuId(Integer puId) {
        this.puId = puId;
    }

    public Integer getElectionId() {
        return electionId;
    }

    public void setElectionId(Integer electionId) {
        this.electionId = electionId;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getAltitude() {
        return altitude;
    }

    public void setAltitude(Double altitude) {
        this.altitude = altitude;
    }

    public Double getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(Double accuracy) {
        this.accuracy = accuracy;
    }

    public String getEc8aUrl() {
        return ec8aUrl;
    }

    public void setEc8aUrl(String ec8aUrl) {
        this.ec8aUrl = ec8aUrl;
    }

    public Integer getRegisteredVoters() {
        return registeredVoters;
    }

    public void setRegisteredVoters(Integer registeredVoters) {
        this.registeredVoters = registeredVoters;
    }

    public Integer getAccreditedVoters() {
        return accreditedVoters;
    }

    public void setAccreditedVoters(Integer accreditedVoters) {
        this.accreditedVoters = accreditedVoters;
    }

    public Integer getValidVotes() {
        return validVotes;
    }

    public void setValidVotes(Integer validVotes) {
        this.validVotes = validVotes;
    }

    public Integer getRejectedVotes() {
        return rejectedVotes;
    }

    public void setRejectedVotes(Integer rejectedVotes) {
        this.rejectedVotes = rejectedVotes;
    }

    public Integer getTotalVotesCast() {
        return totalVotesCast;
    }

    public void setTotalVotesCast(Integer totalVotesCast) {
        this.totalVotesCast = totalVotesCast;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getSimSerial() {
        return simSerial;
    }

    public void setSimSerial(String simSerial) {
        this.simSerial = simSerial;
    }

    public String getMetaInstanceId() {
        return metaInstanceId;
    }

    public void setMetaInstanceId(String metaInstanceId) {
        this.metaInstanceId = metaInstanceId;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Timestamp dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Date getDateModified() {
        return dateModified;
    }

    public void setDateModified(Timestamp dateModified) {
        this.dateModified = dateModified;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Timestamp startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Timestamp endDate) {
        this.endDate = endDate;
    }
   
    public Date getSubmissionDate() {
        return submissionDate;
    }

    public void setSubmissionDate(Timestamp submissionDate) {
        this.submissionDate = submissionDate;
    }
    
            
}
