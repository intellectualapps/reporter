/*
 * To change this license header, choose License Headers in PartyScore Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pw.app.data.manager;

import com.pw.app.data.provider.DataProviderLocal;
import com.pw.app.model.PartyScore;
import javax.ejb.EJB;
import javax.ejb.EJBTransactionRolledbackException;
import javax.ejb.Stateless;

/**
 *
 * @author buls
 */
@Stateless
public class PartyScoreDataManager implements PartyScoreDataManagerLocal {

    @EJB
    private DataProviderLocal crud;    

    @Override
    public PartyScore create(PartyScore partyScore) throws EJBTransactionRolledbackException {
        return crud.create(partyScore);
    }

    @Override
    public PartyScore update(PartyScore partyScore) {
        return crud.update(partyScore);
    }

    @Override
    public PartyScore get(String partyScoreId) {
        return crud.find(partyScoreId, PartyScore.class);
    }

    @Override
    public void delete(PartyScore partyScore) {
        crud.delete(partyScore);
    }
  
}

