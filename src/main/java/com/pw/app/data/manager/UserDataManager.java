/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pw.app.data.manager;

import com.pw.app.data.provider.DataProviderLocal;
import com.pw.app.model.User;
import java.util.HashMap;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.EJBTransactionRolledbackException;
import javax.ejb.Stateless;

/**
 *
 * @author buls
 */
@Stateless
public class UserDataManager implements UserDataManagerLocal {

    @EJB
    private DataProviderLocal crud;    

    @Override
    public User create(User user) throws EJBTransactionRolledbackException {
        return crud.create(user);
    }

    @Override
    public User update(User user) {
        return crud.update(user);
    }

    @Override
    public User get(String userId) {
        return crud.find(userId, User.class);
    }

    @Override
    public void delete(User user) {
        crud.delete(user);
    }
  
    @Override
    public List<User> getByUsername(String username) {        
        HashMap<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("username", username);
        return crud.findByNamedQuery("User.findByUsername", parameters, User.class);
    }    
}

