/*
 * To change this license header, choose License Headers in PartyScore Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pw.app.data.manager;

import com.pw.app.model.PartyScore;
import javax.ejb.EJBTransactionRolledbackException;
import javax.ejb.Local;

/**
 *
 * @author buls
 */
@Local
public interface PartyScoreDataManagerLocal {
    
    PartyScore create(PartyScore partyScore) throws EJBTransactionRolledbackException;

    PartyScore update(PartyScore partyScore);

    PartyScore get(String partyScoreId);

    void delete(PartyScore partyScore);
    
}
