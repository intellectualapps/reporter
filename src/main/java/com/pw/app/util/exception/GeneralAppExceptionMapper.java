/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pw.app.util.exception;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 *
 * @author buls
 */
@Provider
public class GeneralAppExceptionMapper implements ExceptionMapper<GeneralAppException> {  

    @Override
    public Response toResponse(GeneralAppException gae) {
        return Response.status(gae.getStatus())
                .entity(new ErrorMessage(gae))
                .type(MediaType.APPLICATION_JSON).
                build();
    }
    
}
