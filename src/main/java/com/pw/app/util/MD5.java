/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pw.app.util;

import static java.nio.charset.Charset.forName;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 *
 * @author Lateefah
 */
public class MD5 {
    public static String hash(String hashString) {
        try {
            byte[] passByte = hashString.getBytes(forName("UTF-8"));
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(passByte);

            byte messageDigest[] = md.digest();

            StringBuilder hexString = new StringBuilder();

            for (int i = 0; i < messageDigest.length; i++) {
                String hex = Integer.toHexString(0xFF & messageDigest[i]);
                if (hex.length() < 2) {
                    hex = "0" + hex;
                }
                hexString.append(hex);
            }

            return hexString.toString();
        } catch (NoSuchAlgorithmException ex) {
            throw new RuntimeException(ex);
        }
    }
}
