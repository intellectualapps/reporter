/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pw.app.service;


import com.pw.app.manager.PhotoManagerLocal;
import com.pw.app.manager.UserManagerLocal;
import com.pw.app.model.DeviceToken;
import com.pw.app.pojo.AppBoolean;
import com.pw.app.pojo.AppUser;
import com.pw.app.pojo.UserDataAccessPayload;
import com.pw.app.pojo.UserPayload;
import com.pw.app.util.exception.GeneralAppException;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Lateefah
 */
@Stateless
@Path("/v1/photo")
public class PhotoService {
    
    @Context
    HttpServletRequest request;   
    
    @EJB    
    PhotoManagerLocal photoManager;
           
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public Response savePhoto(@FormParam("project-id") String projectId,
                            @FormParam("result-sheet-photo") String resultSheetPhoto,
                            @FormParam("election-photo-1") String electionPhoto1,
                            @FormParam("election-photo-2") String electionPhoto2) throws GeneralAppException {          
        AppBoolean appBoolean = photoManager.savePhotoData(projectId, resultSheetPhoto, 
                electionPhoto1, electionPhoto2);
        return Response.ok(appBoolean).build();           
    }
        
}
