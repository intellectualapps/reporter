/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pw.app.pojo;

import com.pw.app.model.*;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author buls
 */

public class AppProject implements Serializable {
  
    private Integer id;
    private Date submissionDate;
    private String agentId;
    private Integer puId;
    private Integer electionId;
    private Double latitude;
    private Double longitude;    
    private Double altitude;
    private Double accuracy;
    private String ec8aUrl;
    private Integer registeredVoters;
    private Integer accreditedVoters;
    private Integer validVotes;
    private Integer rejectedVotes;
    private Integer totalVotesCast;
    private String deviceId;
    private String simSerial;
    private String metaInstanceId;
    private Date dateCreated;
    private Date dateModified;
    private Date startDate;
    private Date endDate;
        
    public AppProject() { }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public Date getSubmissionDate() {
        return submissionDate;
    }

    public void setSubmissionDate(Date submissionDate) {
        this.submissionDate = submissionDate;
    }

    public Integer getPuId() {
        return puId;
    }

    public void setPuId(Integer puId) {
        this.puId = puId;
    }

    public Integer getElectionId() {
        return electionId;
    }

    public void setElectionId(Integer electionId) {
        this.electionId = electionId;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getAltitude() {
        return altitude;
    }

    public void setAltitude(Double altitude) {
        this.altitude = altitude;
    }

    public Double getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(Double accuracy) {
        this.accuracy = accuracy;
    }

    public String getEc8aUrl() {
        return ec8aUrl;
    }

    public void setEc8aUrl(String ec8aUrl) {
        this.ec8aUrl = ec8aUrl;
    }

    public Integer getRegisteredVoters() {
        return registeredVoters;
    }

    public void setRegisteredVoters(Integer registeredVoters) {
        this.registeredVoters = registeredVoters;
    }

    public Integer getAccreditedVoters() {
        return accreditedVoters;
    }

    public void setAccreditedVoters(Integer accreditedVoters) {
        this.accreditedVoters = accreditedVoters;
    }

    public Integer getValidVotes() {
        return validVotes;
    }

    public void setValidVotes(Integer validVotes) {
        this.validVotes = validVotes;
    }

    public Integer getRejectedVotes() {
        return rejectedVotes;
    }

    public void setRejectedVotes(Integer rejectedVotes) {
        this.rejectedVotes = rejectedVotes;
    }

    public Integer getTotalVotesCast() {
        return totalVotesCast;
    }

    public void setTotalVotesCast(Integer totalVotesCast) {
        this.totalVotesCast = totalVotesCast;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getSimSerial() {
        return simSerial;
    }

    public void setSimSerial(String simSerial) {
        this.simSerial = simSerial;
    }

    public String getMetaInstanceId() {
        return metaInstanceId;
    }

    public void setMetaInstanceId(String metaInstanceId) {
        this.metaInstanceId = metaInstanceId;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Date getDateModified() {
        return dateModified;
    }

    public void setDateModified(Date dateModified) {
        this.dateModified = dateModified;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
   
    
            
}
