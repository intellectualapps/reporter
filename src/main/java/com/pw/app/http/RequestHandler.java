/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pw.app.http;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 *
 * @author buls
 */
public class RequestHandler {
    
    public String executePost(String targetURL, String urlParameters, String accessToken) {
        HttpURLConnection connection = null;

      /*  urlParameters = "{\n" +
"   \"description\":\"Purchase Agreement\",\n" +
"   \"shipping_address\":{\n" +
"      \"line1\":\"Pritech Business Park, Bldg. 11, Outer Ring Road\",\n" +
"      \"city\":\"Bangalore\",\n" +
"      \"state\":\"Karnataka\",\n" +
"      \"postal_code\":\"5600101\",\n" +
"      \"country_code\":\"IN\"\n" +
"   },\n" +
"   \"payer\":{\n" +
"      \"payment_method\":\"paypal\"\n" +
"   },\n" +
"   \"plan\":{\n" +
"      \"type\":\"MERCHANT_INITIATED_BILLING\",\n" +
"      \"merchant_preferences\":{\n" +
"         \"notify_url\":\"http://www.acme.com/payment_status/notify\",\n" +
"         \"cancel_url\":\"http://www.acme.com/payment_status/cancel\",\n" +
"         \"return_url\":\"http://www.acme.com/payment_status/success\",\n" +
"         \"accepted_pymt_type\":\"Instant\",\n" +
"         \"experience_id\":\"1234\",\n" +
"         \"skip_shipping_address\":false,\n" +
"         \"immutable_shipping_address\":false\n" +
"      }\n" +
"   },\n" +
"   \"merchant_custom_data\":\"KEY 64FA256F-7EE1-4E2A-AFFA-A1BD69E7ED2A\"\n" +
"}";*/
        System.out.println(urlParameters);
        try {
            //Create connection
            URL url = new URL(targetURL);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type",
                    "application/json");
            connection.setRequestProperty("Authorization", accessToken);

            //connection.setRequestProperty("Content-Length",
              //      Integer.toString(urlParameters.getBytes().length));
            //connection.setRequestProperty("Content-Language", "en-US");

            connection.setUseCaches(false);
            connection.setDoOutput(true);
            
            

            //Send request
            DataOutputStream wr = new DataOutputStream(
                    connection.getOutputStream());
            wr.writeBytes(urlParameters);
            wr.close();

            //Get Response  
            InputStream is = connection.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is));
            StringBuilder response = new StringBuilder(); // or StringBuffer if Java version 5+
            String line;
            while ((line = rd.readLine()) != null) {
                response.append(line);
                response.append('\r');
            }
            rd.close();
            return response.toString();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
    }
    
}
